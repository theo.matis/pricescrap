import requests
import urllib.request
from bs4 import BeautifulSoup
import csv


def scrapMaxesport(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    s = str(soup.find(class_="price")).replace('<p class="price"><span class="woocommerce-Price-amount amount">','').replace('<span class="woocommerce-Price-currencySymbol">€</span></span></p>','')
    return s
def scrapMateriel(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    s = str(soup.find(class_="o-product__price o-product__price--large"))
    s = s.replace('<span class="o-product__price o-product__price--large">','').replace("<sup>","").replace("</span>","").replace("</sup>","").replace("€",".")
    return s
def scrapTopAchat(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    s = str(soup.find(class_="eproduct CGP"))
    s,sep,tail = s.partition("€")
    del sep,tail
    s = s.split("</span><span>")
    return s[1].replace(" ","")
def scrapAmazon(url):
    response = requests.get(url,headers={"User-Agent":"Maxesport.gg"})
    soup = BeautifulSoup(response.text, "html.parser")
    s = str(soup.find(id="priceblock_ourprice"))
    s = s.replace('<span class="a-size-medium a-color-price priceBlockBuyingPriceString" id="priceblock_ourprice">',"").replace('€</span>','').replace(',',".")
    return s 
with open('table_produit.csv', 'w+', newline='') as table:
    fieldnames = ['Nom','Maxesport.gg prix', 'Materiel.net prix',"Topachat.com prix","Amazon.fr prix"]
    writer = csv.DictWriter(table, fieldnames=fieldnames)
    writer.writeheader()
    with open('liens_produit.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            writer.writerow({"Nom": row["\ufeffname"],"Maxesport.gg prix": scrapMaxesport(row["maxesport.gg"]),"Materiel.net prix": scrapMateriel(row["materiel.net"]),"Topachat.com prix": scrapTopAchat(row["topachat.com"]),"Amazon.fr prix": scrapAmazon(row["amazon.fr"])})
